import Vue from 'vue'
import Router from 'vue-router'
import MainPage from '@/components/MainPage'
import ResultPage from '@/components/ResultPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MainPage',
      component: MainPage
    },
    {
      path: '/search',
      name: 'ResultPage',
      component: ResultPage
    }
  ]
})
